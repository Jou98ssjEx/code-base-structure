================================================
====================Resourse====================
================================================

///////////////////IMG////////////////////

================================================
Remove the background from your image
https://www.remove.bg/
================================================

================================================
Blob Generator
https://www.blobmaker.app/
================================================

///////////////////COLOR////////////////////

================================================
HSL color mode
https://www.w3schools.com/colors/colors_hsl.asp
================================================

///////////////////ICONS////////////////////

================================================
BOX ICONS
https://boxicons.com/
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
================================================

================================================
UNICONS
https://iconscout.com/unicons/explore/line
<link rel="stylesheet" href="https://unicons.iconscout.com/release/v4.0.0/css/line.css">
Practice cv Alexa
================================================

///////////////////PDF////////////////////

================================================
HTML2 PDF
https://ekoopmans.github.io/html2pdf.js/
Practice cv
================================================


///////////////////SLIDES////////////////////

================================================
SWIPER
https://swiperjs.com/demos#css-mode
Practice cv Alexa
================================================


///////////////////STYLES////////////////////

================================================
BOOTSTRAP
https://getbootstrap.com/docs/5.0/getting-started/introduction/
================================================

///////////////////FONTS////////////////////

================================================
Google Fonts
https://fonts.google.com/
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600&display=swap');
================================================
